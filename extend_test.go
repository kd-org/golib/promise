package promise

import (
	"context"
	"errors"
	"io"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestResolve(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	p := Resolve("foo")
	require.NotNil(t, p)
	value, err := p.Await(ctx)
	require.NoError(t, err)
	require.NotNil(t, value)
	require.EqualValues(t, "foo", *value)
}

func TestReject(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	p := Reject[any](io.EOF)
	require.NotNil(t, p)
	_, err := p.Await(ctx)
	require.True(t, errors.Is(err, io.EOF))
}
