package promise

func Resolve[T any](value T) *Promise[T] {
	return &Promise[T]{
		value: &value,
		ch:    ClosedChannel,
	}
}

func Reject[T any](err error) *Promise[T] {
	return &Promise[T]{
		err: err,
		ch:  ClosedChannel,
	}
}

var ClosedChannel = func() chan struct{} {
	channel := make(chan struct{})
	close(channel)
	return channel
}()
